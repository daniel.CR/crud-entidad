package com.danielcr.springboot.backend.apirest.models.dao.models.services;

import com.danielcr.springboot.backend.apirest.models.entity.Usuario;


public interface IUsuarioService {
	
	public Usuario findByUsername2(String username);

}
