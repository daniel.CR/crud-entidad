-- MySQL Script generated by MySQL Workbench
-- Wed Apr 20 20:37:40 2022
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema prueba
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `prueba` ;

-- -----------------------------------------------------
-- Schema prueba
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `prueba` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `prueba` ;

-- -----------------------------------------------------
-- Table `prueba`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prueba`.`roles` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `UK_ldv0v52e0udsh2h1rs0r0gw1n` ON `prueba`.`roles` (`nombre` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `prueba`.`tb_tipo_contribuyente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prueba`.`tb_tipo_contribuyente` (
  `id_tipo_contribuyente` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  `estado` BIT(1) NOT NULL,
  PRIMARY KEY (`id_tipo_contribuyente`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `prueba`.`tb_tipo_documento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prueba`.`tb_tipo_documento` (
  `id_tipo_documento` INT NOT NULL AUTO_INCREMENT,
  `codigo` VARCHAR(20) NOT NULL,
  `nombre` VARCHAR(100) NOT NULL,
  `descripcion` VARCHAR(200) NULL DEFAULT NULL,
  `estado` BIT(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_tipo_documento`))
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8mb3;


-- -----------------------------------------------------
-- Table `prueba`.`tb_entidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prueba`.`tb_entidad` (
  `id_entidad` INT NOT NULL AUTO_INCREMENT,
  `id_tipo_documento` INT NOT NULL,
  `nro_documento` VARCHAR(25) NOT NULL,
  `razon_social` VARCHAR(100) NOT NULL,
  `nombre_comercial` VARCHAR(100) NULL DEFAULT NULL,
  `id_tipo_contribuyente` INT NULL DEFAULT NULL,
  `direccion` VARCHAR(250) NULL DEFAULT NULL,
  `telefono` VARCHAR(50) NULL DEFAULT NULL,
  `estado` BIT(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id_entidad`),
  CONSTRAINT `tb_entidad_ibfk_2`
    FOREIGN KEY (`id_tipo_contribuyente`)
    REFERENCES `prueba`.`tb_tipo_contribuyente` (`id_tipo_contribuyente`),
  CONSTRAINT `tb_entidad_ibfk_3`
    FOREIGN KEY (`id_tipo_documento`)
    REFERENCES `prueba`.`tb_tipo_documento` (`id_tipo_documento`))
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8mb3;

CREATE UNIQUE INDEX `nro_documento_UNIQUE` ON `prueba`.`tb_entidad` (`nro_documento` ASC) VISIBLE;

CREATE INDEX `fk_tb_entidad_tb_tipo_documento1_idx` ON `prueba`.`tb_entidad` (`id_tipo_documento` ASC) VISIBLE;

CREATE INDEX `fk_tb_entidad_tb_tipo_contribuyente1_idx` ON `prueba`.`tb_entidad` (`id_tipo_contribuyente` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `prueba`.`usuarios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prueba`.`usuarios` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `apellido` VARCHAR(255) NULL DEFAULT NULL,
  `email` VARCHAR(255) NULL DEFAULT NULL,
  `enabled` BIT(1) NULL DEFAULT NULL,
  `nombre` VARCHAR(255) NULL DEFAULT NULL,
  `password` VARCHAR(60) NULL DEFAULT NULL,
  `username` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `UK_kfsp0s1tflm1cwlj8idhqsad0` ON `prueba`.`usuarios` (`email` ASC) VISIBLE;

CREATE UNIQUE INDEX `UK_m2dvbwfge291euvmk6vkkocao` ON `prueba`.`usuarios` (`username` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `prueba`.`usuarios_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `prueba`.`usuarios_roles` (
  `usuario_id` BIGINT NOT NULL,
  `role_id` BIGINT NOT NULL,
  CONSTRAINT `FKihom0uklpkfpffipxpoyf7b74`
    FOREIGN KEY (`role_id`)
    REFERENCES `prueba`.`roles` (`id`),
  CONSTRAINT `FKqcxu02bqipxpr7cjyj9dmhwec`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `prueba`.`usuarios` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE UNIQUE INDEX `UKqjaspm7473pnu9y4jxhrds8r2` ON `prueba`.`usuarios_roles` (`usuario_id` ASC, `role_id` ASC) VISIBLE;

CREATE INDEX `FKihom0uklpkfpffipxpoyf7b74` ON `prueba`.`usuarios_roles` (`role_id` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
