import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReglasComponent } from './shared/components/reglas/reglas.component';

const routes: Routes = [
    {path:'',redirectTo: "/entidades",pathMatch:"full"},
    {path:'reglas',component:ReglasComponent},

   { path: 'entidades', loadChildren: () => import('./modules/entidades/entidades.module').then(m => m.EntidadesModule) },
    { path: 'login', loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule) },

 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
